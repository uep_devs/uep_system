<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sysnum');
            $table->string('faculty_id');
            $table->integer('subj_id');
            $table->string('day');
            $table->string('time');
            $table->string('room');
            $table->string('school_yr');
            $table->string('semester');
            $table->string('block');
            $table->string('course_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_schedules');
    }
}
