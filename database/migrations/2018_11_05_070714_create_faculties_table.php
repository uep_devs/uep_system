<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacultiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_faculties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('faculty_id');
            $table->dateTime('doc_date');
            $table->string('lname');
            $table->string('fname');
            $table->string('mname');
            $table->string('ename');
            $table->string('position');
            $table->integer('status');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_faculties');
    }
}
