<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_subjects', function (Blueprint $table) {
            $table->increments('id')->primary;
            $table->integer('sys_num');
            $table->string('yr_lvl');
            $table->string('semester');
            $table->string('course_code');
            $table->string('course');
            $table->string('major');
            $table->string('description');
            $table->integer('lec');
            $table->integer('lab');
            $table->string('tempsched');
            $table->integer('type');
            $table->string('block');
            $table->string('code');
            $table->integer('coden');
            $table->integer('regular');
            $table->string('course_id');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_subjects');
    }
}
