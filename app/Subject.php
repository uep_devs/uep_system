<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    // Protected $table = 'tbl_subjects';
    Protected $table = 'osbj';

    Public function faculty()
    {
        return $this->belongsTo('App\Faculty');
    }

    Public function sched()
    {
        return $this->belongsTo('App\Schedule');
    }
    
}
