<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    // Table Name
    Protected $table = 'osched';
    // Primary Key
    public $primaryKey = 'SysNum';
    // Timestamps
    public $timestamps = true;

    public function subject()
    {
        return $this->hasMany('App\Subject', 'SysNum');
    }

    // public function faculty()
    // {
    //     // return $this->belongsTo('App\Faculty');
    // }

}
