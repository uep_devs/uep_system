<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Schedule extends Model
{
    Protected $table = 'tbl_schedules';

    Public function sched()
    {
        return $this->hasMany('App\Subject', 'sys_num');
    }
}
