<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;
use App\Faculty;
use App\Subject;
use DB;

class ScheduleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Schedule::all();
        // $sched = Schedule::all();
        // $instructor = Faculty::select(DB::raw("CONCAT(ofac.lname,' ',ofac.fname) as instructor"))->get();
        $sched = DB::table('osched as a')
            ->select('a.SysNum as id', 'b.Description as description', 'b.CourseCode as course_code', 'b.Course as course', 'a.Day as day', 'a.Time as time', 'a.Block as block', 'a.Room as room', 'a.SchoolYr as school_yr', 'a.Semester as semester', 'b.YrLvl as yr_lvl', 'b.SysNum as subj_num', 'b.Major as major', 'c.Lname as lname', 'c.Fname as fname', 'c.Mname as mname')
            ->join('osbj as b', 'a.SysOsbj', '=', 'b.SysNum')
            ->join('ofac as c', 'a.FacultyID', '=', 'c.FacultyID')
            ->orderBy('a.SysNum', 'desc')
            ->get();

        $subj = DB::table('osbj')
                ->select('*')
                ->orderBy('Course', 'asc')
                ->get();
            

            // dd($instructor);

        // $sched = DB::table('tbl_schedules as a')
        //     ->select('a.id as id', 'b.description as description', 'b.course_code as course_code', 'b.course as course', 'a.day as day', 'a.time as time', 'a.block as block', 'a.room as room', 'a.school_yr as school_yr', 'a.semester as semester', 'b.yr_lvl as yr_lvl', 'b.sys_num as subj_num', 'b.major as major')
        //     ->join('tbl_subjects as b', 'a.subj_id', '=', 'b.sys_num')
        //     ->join('tbl_faculties as c', 'a.faculty_id', '=', 'c.faculty_id')
        //     ->get();
        // $sched = Schedule::orderBy('CourseCode', 'desc')->paginate(10);
        
        // return view('erp_pages.scheduling.schedule')->with('schedule', $sched);
        return view('erp_pages.scheduling.schedule', ['schedule'=>$sched, 'subject'=>$subj]);
    }

    function test_query()
    {
        // $data2 = Schedule::select(DB::raw('ofac.LName'))->get();
        // $data = Subject::select(DB::raw('SysNum'))->get();
        // $data = Schedule::orderBy('SysNum', 'desc')->paginate(20);
        $data = Schedule::all();
        // echo "<pre>";
        return view('erp_pages.scheduling.schedule')->with('schedule', $data);
        // print_r($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // echo $id;
        $schedid = Schedule::findOrFail($request->sched_id);
        $schedid->delete();
        // DB::table('osched')->where('SysNum', $request->sched_id)->delete();

        return back();
        // dd($request->sched_id);
    }
}
