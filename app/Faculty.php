<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    // protected $table = 'tbl_faculties';
    protected $table = 'ofac';

    public function subject()
    {
        return $this->hasMany('App\Subject');
    }
    
    // Protected $table = 'ofac';
    
    // Public function sched()
    // {
    //     return $this->belongsTo('App\Schedule');
    // }
}
