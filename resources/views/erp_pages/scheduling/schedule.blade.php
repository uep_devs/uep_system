@extends('layouts.default')

@section('title', 'Subject Scheduling')

@push('css')
    <link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
    <link href="/assets/plugins/datatables/css/select/select.bootstrap4.min.css" rel="stylesheet" />
    <link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />

    <link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
	<link href="/assets/plugins/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" />
	<link href="/assets/plugins/ion-rangeslider/css/ion.rangeSlider.skinNice.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" />
	<link href="/assets/plugins/password-indicator/css/password-indicator.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-combobox/css/bootstrap-combobox.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
	<link href="/assets/plugins/tag-it/css/jquery.tagit.css" rel="stylesheet" />
    <link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    <link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="/assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css" rel="stylesheet" />
    <link href="/assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.css" rel="stylesheet" />
    <link href="/assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker-fontawesome.css" rel="stylesheet" />
    <link href="/assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker-glyphicons.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item active">Subject Scheduling</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Subject Scheduling <small></small></h1>
	<!-- end page-header -->
	
	<!-- begin panel -->
	<div class="panel panel-inverse">
		<div class="panel-heading">
                
			<div class="panel-heading-btn">
                <!-- Add button -->
                <a href="#modal-dialog-create" data-toggle="modal" class="btn btn-default btn-xs">
                        <i class="fas fa-pencil-alt"></i> &nbsp;Create New
                </a>
                <!-- End Add button -->
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
			</div>
            <h4 class="panel-title"><i class="fas fa-calendar-alt"></i> &nbsp;All Schedule Lists</h4>
            
        </div>
        <!-- begin alert -->
            <div class="alert alert-secondary fade hide">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
                Message Here...
            </div>
        <!-- end alert -->
        <!-- begin panel-body -->
        
        <div class="panel-body">
            <table id="data-table-select" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="1%">ID</th>
                        <th data-orderable="false">Action</th>
                        <th>Code</th>
                        <th>Description</th>
                        <th>Course</th>
                        <th>Day</th>
                        <th>Time</th>
                        <th>Room</th>
                        <th>Block</th>
                        <th>Instructor</th>
                        <th class="text-nowrap">School Year</th>
                        <th>Semester</th>
                        <th class="text-nowrap">Year Level</th>
                        <th>Subj Num</th>
                        <th>Major</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($schedule) > 0)
                        @foreach ($schedule as $schedules)
                            <tr>
                                <td class="f-s-600 text-inverse">{{ $schedules->id }}</td>
                                <td class="text-nowrap"><a href="#modal-dialog-edit" data-sysnum="{{$schedules->id}}" data-subj="{{$schedules->course_code}} - {{$schedules->description}}" data-toggle="modal"><i class="fas fa-edit fa-sm"></i></a> | <a href="#modal-alert" data-sysnum="{{$schedules->id}}" data-toggle="modal"><i class="fas fa-trash-alt fa-sm"></i></a></td>
                                <td class="text-nowrap">{{$schedules->course_code}}</td>
                                <td class="text-nowrap">{{$schedules->description}}</td>
                                <td>{{$schedules->course}}</td>
                                <td>{{$schedules->day}}</td>
                                <td class="text-nowrap">{{$schedules->time}}</td>
                                <td class="text-nowrap">{{$schedules->room}}</td>
                                <td width="1%">{{$schedules->block}}</td>
                                <td class="text-nowrap">{{$schedules->lname}}, {{$schedules->fname}} {{$schedules->mname}}</td>
                                <td class="text-nowrap">{{$schedules->school_yr}}</td>
                                <td class="text-nowrap">{{$schedules->semester}}</td>
                                <td>{{$schedules->yr_lvl}}</td>
                                <td>{{$schedules->subj_num}}</td>
                                <td>{{$schedules->major}}</td>
                            </tr>
                        @endforeach
                    @else
                        {{-- No Data --}}
                    @endif
                </tbody>
            </table>
            <!-- #modal-dialog -->
                {{-- #modal-create-new --}}
                <div class="modal fade" id="modal-dialog-create">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Create New</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="subj">Course Subject</label>
                                        <select class="combobox" data-live-search="true" data-style="btn-white" name="subj23" id="subj23" value="">
                                                <option value="" selected>- Course Subject -</option>
                                            @foreach ($subject as $subjects)
                                        <option value="{{$subjects->SysNum}}">{{$subjects->Course}} - [{{$subjects->CourseCode}}]   {{$subjects->Description}}</option>
                                                
                                            @endforeach
                                            
                                        </select>
                                        {{-- {!! Form::select('subj', $schedule->pluck('subj'), $schedule->w)} --}}
                                    </div>

                                     
                                    
                                    <!-- begin panel-body -->
                                    {{-- <div class="form-group">
                                        <form class="form-horizontal form-bordered">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label">Course Subject</label>
                                                <div class="col-md-8">
                                                        <select class="combobox" data-live-search="true" data-style="btn-white" name="subj23" id="subj23" value="">
                                                                <option value="" selected>- Course Subject -</option>
                                                            @foreach ($subject as $subjects)
                                                        <option value="{{$subjects->SysNum}}">{{$subjects->Course}} - [{{$subjects->CourseCode}}]   {{$subjects->Description}}</option>                     
                                                            @endforeach          
                                                        </select>
                                                </div>
                                            </div>
                                        </form>
                                    </div> --}}
                                    <!-- end panel-body -->
                             
                                    
                                    <div class="form-group">
                                        <label for="sysnum">System Number</label>
                                        <input type="text" class="form-control" name="sysnum" id="sysnum">
                                    </div>
                                    <div class="form-group">
                                            <label for="time">Time</label>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <div class="input-group date" id="datetimepicker2">
                                                        <input type="text" class="form-control" />
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-clock"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="input-group date" id="datetimepicker2">
                                                        <input type="text" class="form-control" />
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-clock"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        
                                        {{-- <label for="time">Time</label>
                                        <input type="text" class="form-control" name="time" id="time"> --}}
                                        {{-- <label for="time">Time</label>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <div class="input-group date" id="datetimepicker2">
                                                    <input type="text" class="form-control" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <span class="input-group-addon">to</span> 
                                            
                                        </div>--}}
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-md-3 col-form-label">Days</label>
                                        <div class="col-md-9">
                                            <div class="checkbox checkbox-css checkbox-inline">
                                                <input type="checkbox" value="M" id="inlineCssCheckbox1" checked />
                                                <label for="inlineCssCheckbox1">Monday</label>
                                            </div>
                                            <div class="checkbox checkbox-css checkbox-inline">
                                                <input type="checkbox" value="T" id="inlineCssCheckbox2" checked/>
                                                <label for="inlineCssCheckbox2">Tuesday</label>
                                            </div>
                                            <div class="checkbox checkbox-css checkbox-inline">
                                                <input type="checkbox" value="W" id="inlineCssCheckbox3" checked />
                                                <label for="inlineCssCheckbox3">Wednesday</label>
                                            </div>
                                            <div class="checkbox checkbox-css checkbox-inline">
                                                <input type="checkbox" value="Th" id="inlineCssCheckbox4" checked/>
                                                <label for="inlineCssCheckbox4">Thursday</label>
                                            </div>
                                            <div class="checkbox checkbox-css checkbox-inline">
                                                <input type="checkbox" value="F" id="inlineCssCheckbox5" checked/>
                                                <label for="inlineCssCheckbox5">Friday</label>
                                            </div>
                                            <div class="checkbox checkbox-css checkbox-inline">
                                                <input type="checkbox" value="Sat" id="inlineCssCheckbox6" />
                                                <label for="inlineCssCheckbox6">Saturday</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                            <div class="col-md-4">
                                        <label for="room">Room</label>
                                        <input type="text" class="form-control" name="room" id="room">
                                    </div>
                                    
                                    
                                            <div class="col-md-4">
                                            <label for="subj">School Year</label>
                                            <select class="form-control selectpicker" data-live-search="true" data-style="btn-white" name="sy" id="sy" value="">
                                                    <option value="" selected>- School Year -</option>
                                                
                                                    <option value="2019-2020">2019-2020</option>
                                                    <option value="2018-2019">2018-2019</option>
                                                    <option value="2017-2018">2017-2018</option>
                                                    <option value="2016-2017">2016-2017</option>
                                                
                                            </select>
                                        </div>
                                            {{-- {!! Form::select('subj', $schedule->pluck('subj'), $schedule->w)} --}}
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <a href="javascript:;" class="btn btn-success">Save Changes</a>
                                    <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- end modal create-new --}}
            <div class="modal fade" id="modal-dialog-edit">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Edit</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="subj">Course Subject</label>
                                {{-- <input type="text" class="form-control" name="subj1" id="subj1"> --}}
                                <select class="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white" name="subj" id="subj" value="">
                                    {{-- <option value="" selected>Select a course subject</option> --}}
                                    {{-- <option value="" selected></option> --}}
                                    <option value="subj" selected></option>
                                    <option value="AF">Afghanistan</option>
                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AS">American Samoa</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>
                                    <option value="AQ">Antarctica</option>
                                    <option value="AG">Antigua and Barbuda</option>
                                    
                                </select>
                                {{-- {!! Form::select('subj', $schedule->pluck('subj'), $schedule->w)} --}}
                            </div>

                           
                            
                            <div class="form-group">
                                <label for="sysnum">System Number</label>
                                <input type="text" class="form-control" name="sysnum" id="sysnum">
                            </div>
                            <div class="form-group">
                                <label for="time">Time</label>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <div class="input-group date" id="datetimepicker2">
                                            <input type="text" class="form-control" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-clock"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="input-group-addon">to</span>
                                    <div class="col-md-4">
                                        <div class="input-group date" id="datetimepicker2">
                                            <input type="text" class="form-control" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-clock"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="javascript:;" class="btn btn-success">Save Changes</a>
                            <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal modal-danger fade" id="modal-alert">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Alert Message</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                    <form action="{{route('scheduling.destroy', 'test')}}" method="post">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="alert alert-danger m-b-0">
                                <h5><i class="fa fa-info-circle"></i> Delete Confirmation</h5>
                                <p>Are you sure you want to delete this?</p>
                                <input type="hidden" name="sched_id" id="sched_id" value="">
                            </div>
                        </div>
                        <div class="modal-footer">
                            {{-- <a href="javascript:;" class="btn btn-danger" data-dismiss="modal">Yes, Delete</a> --}}
                            <button type="submit" class="btn btn-danger">Yes, Delete</button>
                            <button type="button" class="btn btn-white" data-dismiss="modal">No, Cancel</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
            {{-- end modal --}}
        </div>
            <!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
    <script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
    <script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
    <script src="/assets/plugins/datatables/js/select/dataTables.select.js"></script>
    <script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
    <script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
    <script src="/assets/js/demo/table-manage-select.demo.js"></script>
    
    <script src="/assets/plugins/gritter/js/jquery.gritter.js"></script>
	<script src="/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/assets/js/demo/ui-modal-notification.demo.js"></script>
    
    <script src="/assets/plugins/jquery-migrate/jquery-migrate.min.js"></script>
	<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="/assets/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
	<script src="/assets/plugins/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<script src="/assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
	<script src="/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
	<script src="/assets/plugins/password-indicator/js/password-indicator.js"></script>
	<script src="/assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js"></script>
	<script src="/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
	<script src="/assets/plugins/tag-it/js/tag-it.min.js"></script>
    <script src="/assets/plugins/moment/moment.js"></script>
    <script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
    <script src="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="../assets/plugins/bootstrap-show-password/bootstrap-show-password.js"></script>
    <script src="/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
    <script src="/assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.js"></script>
    <script src="/assets/plugins/clipboard/dist/clipboard.min.js"></script>
	<script src="/assets/js/demo/form-plugins.demo.js"></script>

	<script>
        $(document).ready(function() {
            TableManageSelect.init();
            Notification.init();
            FormPlugins.init();
        });
    </script>
@endpush