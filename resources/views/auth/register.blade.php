@extends('layouts.empty', ['paceTop' => true, 'bodyExtraClass' => 'bg-white'])

@section('title', 'Register Page')

@section('content')
<!-- begin register -->

<div class="register register-with-news-feed">
	<!-- begin news-feed -->
	<div class="news-feed">
		<div class="news-image" style="background-image: url(../assets/img/login-bg/login-bg-11.jpg)"></div>
		<div class="news-caption">
			<h4 class="caption-title"><b>UEP</b> - ERP System</h4>
			<p>
				<i>UEP ang Galing!</i>
			</p>
		</div>
	</div>
	<!-- end news-feed -->
	<!-- begin right-content -->
	<div class="right-content">
		<!-- begin register-header -->
		<h1 class="register-header">
			Sign Up
			<small>Create your UEP - ERP System Account.</small>
		</h1>
		<!-- end register-header -->
		<!-- begin register-content -->
		<div class="register-content">
				
			<form method="POST" action="{{ route('register') }}" class="margin-bottom-0">
				@csrf
				<label class="control-label">Full Name <span class="text-danger">*</span></label>
				<div class="row m-b-15">
					<div class="col-md-12">
						{{-- <input type="text" class="form-control" placeholder="First name" required /> --}}
						<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Full Name">

							@if ($errors->has('name'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
							@endif
					</div>
					{{-- <div class="col-md-6 m-b-15">
						<input type="text" class="form-control" placeholder="Last name" required />
						<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Last name" >

							@if ($errors->has('email'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
					</div> --}}
				</div>

				<label class="control-label">Position <span class="text-danger">*</span></label>
				<div class="row m-b-15">
					<div class="col-md-12">
						<input id="position" type="text" class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" name="position" value="{{ old('position') }}" required placeholder="Position" >

							@if ($errors->has('position'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('position') }}</strong>
								</span>
							@endif
					</div>
				</div>

				<label class="control-label">Email <span class="text-danger">*</span></label>
				<div class="row m-b-15">
					<div class="col-md-12">
						{{-- <input type="text" class="form-control" placeholder="Email address" required /> --}}
						<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email address" >

							@if ($errors->has('email'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
					</div>
				</div>
				<label class="control-label">Password <span class="text-danger">*</span></label>
				<div class="row m-b-15">
					<div class="col-md-12">
							<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

							@if ($errors->has('password'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
					</div>
				</div>
				<label class="control-label">Confirm Password <span class="text-danger">*</span></label>
				<div class="row m-b-15">
					<div class="col-md-12">
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm password">
					</div>
				</div>
				<div class="checkbox checkbox-css m-b-30">
					<div class="checkbox checkbox-css m-b-30">
						<input type="checkbox" id="agreement_checkbox" value="">
						<label for="agreement_checkbox">
							By clicking Sign Up, you agree to our <a href="javascript:;">Terms</a> and that you have read our <a href="javascript:;">Data Policy</a>, including our <a href="javascript:;">Cookie Use</a>.
						</label>
					</div>
				</div>
				<div class="register-buttons">
					<button type="submit" class="btn btn-primary btn-block btn-lg">Sign Up</button>
				</div>
				<div class="m-t-20 m-b-40 p-b-40 text-inverse">
					Already a member? Click <a href="/">here</a> to login.
				</div>
				<hr />
				<p class="text-center">
					&copy; UEP WebDev. All Rights Reserved <?php echo date("Y"); ?>
				</p>
			</form>
		</div>
		<!-- end register-content -->
	</div>
	<!-- end right-content -->
</div>
<!-- end register -->
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
