@extends('layouts.empty', ['paceTop' => true, 'bodyExtraClass' => 'bg-white'])

@section('title', 'Login Page')

@section('content')
	<!-- begin login -->
	<div class="login login-with-news-feed">
		<!-- begin news-feed -->
		<div class="news-feed">
			<div class="news-image" style="background-image: url(../assets/img/login-bg/login-bg-11.jpg)"></div>
			<div class="news-caption">
				<h4 class="caption-title"><b>UEP</b> - ERP System</h4>
				<p><i>UEP ang Galing!</i></p>
			</div>
		</div>
		<!-- end news-feed -->
		<!-- begin right-content -->
		<div class="right-content">
			<!-- begin login-header -->
			<div class="login-header">
				<div class="brand">
					<span class="logo1"><img src="../assets/img/login-bg/ueplogo.png" style="height:50px" /></span> <b>UEP</b> - ERP System
					<small></small>
				</div>
				<div class="icon">
					<i class="fa fa-sign-in"></i>
				</div>
			</div>
			<!-- end login-header -->
			<!-- begin login-content -->
			<div class="login-content">
				<form action="{{ route('login') }}" method="POST" class="margin-bottom-0">
						@csrf
					<div class="form-group m-b-15">
                        {{-- <input type="text" class="form-control form-control-lg" placeholder="Email Address" required /> --}}
                        <input id="email" type="email" class="form-control form-control-lg" name="email" value="{{ old('email') }}" required autofocus  placeholder="Email Address" >

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
					</div>
					<div class="form-group m-b-15">
                        {{-- <input type="password" class="form-control form-control-lg" placeholder="Password" required /> --}}
                        <input id="password" type="password" class="form-control form-control-lg" name="password" required placeholder="Password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
					</div>
					<div class="checkbox checkbox-css m-b-30">
						<input type="checkbox" id="remember_me_checkbox" value="" />
						<label for="remember_me_checkbox">
							Remember Me
						</label>
                        
					</div>
					<div class="login-buttons">
						<button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
					</div>
					<div class="m-t-20 m-b-40 p-b-40 text-inverse">
						Not a member yet? Click <a href="{{ route('register') }}" class="text-success">here</a> to register.
					</div>
					<hr />
					<p class="text-center text-grey-darker">
						&copy; UEP WebDev. All Rights Reserved <?php echo date("Y");?>
					</p>
				</form>
			</div>
			<!-- end login-content -->
		</div>
		<!-- end right-container -->
	</div>
	<!-- end login -->
@endsection
