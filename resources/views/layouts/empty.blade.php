<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	@include('includes.head')
</head>
@php
	$bodyClass = (!empty($boxedLayout)) ? 'boxed-layout ' : '';
	$bodyClass .= (!empty($paceTop)) ? 'pace-top ' : '';
	$bodyClass .= (!empty($bodyExtraClass)) ? $bodyExtraClass . ' ' : '';
@endphp
<body class="{{ $bodyClass }}">
	
		<div style="text-align:right;">
			@include('includes.messages')
		</div>
		@include('includes.component.page-loader')
		<div class="app">
			@yield('content')
		</div>
		@include('includes.page-js')
	
</body>
</html>
