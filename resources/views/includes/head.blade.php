<meta charset="utf-8" />
<title>UEP - ERP System | @yield('title')</title>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- ================== BEGIN BASE CSS STYLE ================== -->
{{-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet" /> --}}
{{-- <link href="/assets/css/default/fonts.css" rel="stylesheet" />
<link href="/assets/css/default/fontawesome.css" rel="stylesheet" /> --}}

<link href="/assets/css/fontawesome/css/all.css" rel="stylesheet" />



<link href="/assets/css/bundle.css" rel="stylesheet" />
<link href="/assets/css/default/style.min.css" rel="stylesheet" />
<link href="/assets/css/default/style-responsive.min.css" rel="stylesheet" />
<link href="/assets/css/default/theme/default.css" rel="stylesheet" id="theme" />
<!-- ================== END BASE CSS STYLE ================== -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="/assets/plugins/pace/pace.min.js"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script> --}}
<!-- Scripts -->
{{-- <script src="/js/app.js" defer></script> --}}
<!-- ================== END BASE JS ================== -->

@stack('css')
