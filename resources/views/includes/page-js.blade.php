<script src="/assets/js/bundle.js"></script>
<script src="/assets/js/theme/default.js"></script>
<script src="/assets/js/apps.min.js"></script>

<script>
	$(document).ready(function() {
		App.init();
	});
</script>

<script>
	$('#modal-dialog-create').on('show.bs.modal', function (event){
		var button = $(event.relatedTarget)
		var subj = button.data('subj')
		var sysnum = button.data('sysnum')
		var modal = $(this)

		// modal.find('.modal-body #sysnum').val(sysnum);
		// modal.find('.modal-body #subj23').append('<option value="'+subj+'">'+subj+'</option>');
	})
	$('#modal-dialog-edit').on('show.bs.modal', function (event) {
		
		var button = $(event.relatedTarget)
		var subj = button.data('subj')
		var sysnum = button.data('sysnum')
		var modal = $(this)

		modal.find('.modal-body #sysnum').val(sysnum);
		modal.find('.modal-body #subj').append('<option value="'+subj+'">'+subj+'</option>');

	})
	$('#modal-alert').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)
		var schedid = button.data('sysnum')
		var modal = $(this)
		modal.find('.modal-body #sched_id').val(schedid);
	})
</script>

@stack('scripts')